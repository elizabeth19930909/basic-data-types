lst = []
scorelist = []
for _ in range(int(raw_input())):
    name = raw_input()
    score = float(raw_input())
    lst.append([name, score])

second = sorted(list(set([score for name, score in lst])))[1]
print '\n'.join([name for name, score in lst if score == second])



